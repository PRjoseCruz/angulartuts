var myApp = angular.module('myApp', ['ngRoute']);

myApp.config(function ($routeProvider) {
    
    $routeProvider
    
    .when('/', {
        templateUrl: 'pages/main.html',
        controller: 'mainController'
    })
    
    .when('/second', {
        templateUrl: 'pages/second.html',
        controller: 'secondController'
    })
    
    .when('/second/:num', {
        templateUrl: 'pages/second.html',
        controller: 'secondController'
    })
    
});

myApp.controller('mainController', ['$scope', '$log', function($scope, $log) {
    
    $scope.person = {

      name: 'Jose Cruz',
      address: '0 calle A 18-24',
      zona: 'Zona 15',
      city: 'Guatemala',
      pais: 'Guatemala'

    };

    $scope.fullAddress = function(person) {

      return person.address + ', ' + person.zona + ', ' + person.city + ', ' + person.pais;

    };
    
}]);

myApp.controller('secondController', ['$scope', '$log', '$routeParams', function($scope, $log, $routeParams) {
    
    
    
}]);

myApp.directive("searchResult", function() {
   return {
       restrict: 'AECM',
       templateUrl: 'directives/searchresult.html',
       replace: true,
       scope: {
        personObject: "=",
        fullAddressFunction: "&"
       }
   }
});
