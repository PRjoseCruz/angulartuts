/*global angular*/
var myApp = angular.module('myApp', []);

myApp.controller('mainController', function ($scope) {
    
    $scope.name = 'John Wick';
    $scope.occupation = 'Private sector';
    console.log($scope);
    
});